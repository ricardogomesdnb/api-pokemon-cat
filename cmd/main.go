package main

import (
	"context"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"

	"github.com/gin-gonic/gin"
	"github.com/joho/godotenv"
	"gitlab.com/ricardogomesdnb/api-pokemon-cat/internal/api"
)

func init() {
	err := godotenv.Load()
	if err != nil {
		log.Fatal("Error loading .env file")
	}
}

func main() {
	router := gin.Default()
	router.GET("/", api.HealthCheck)
	router.GET("/api/v1/pokemon/:pokemonID", api.GetPokemonByID)

	server := &http.Server{
		Addr:    ":" + os.Getenv("API_PORT"),
		Handler: router,
	}

	go startAPIHttpServer(server)

	// Wait for interrupt signal to gracefully shutdown the server with
	// a timeout of 5 seconds.
	signalsChannel := make(chan os.Signal, 1)
	signal.Notify(signalsChannel, syscall.SIGTERM)

	// Wait until interrupt signal is given
	<-signalsChannel
	log.Println("Server is shutting down, got interrupt signal")

	if err := server.Shutdown(context.Background()); err != nil {
		log.Fatalln("Unable to gracefully shutdown", err)
	}
}

func startAPIHttpServer(server *http.Server) {
	log.Println("Starting http server, port", os.Getenv("API_PORT"))
	err := server.ListenAndServe()
	if err != nil && err != http.ErrServerClosed {
		log.Fatalln("Unable to start api http server", err)
	}
}
