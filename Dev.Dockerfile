FROM golang:1.16.0-stretch

WORKDIR /app 

RUN curl -sSfL https://raw.githubusercontent.com/cosmtrek/air/master/install.sh | sh -s

CMD ["./bin/air", "-c", "air.toml"]