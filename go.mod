module gitlab.com/ricardogomesdnb/api-pokemon-cat

go 1.16

require (
	github.com/disintegration/imaging v1.6.2
	github.com/gin-gonic/gin v1.7.2
	github.com/joho/godotenv v1.3.0
	golang.org/x/image v0.0.0-20210628002857-a66eb6448b8d // indirect
	golang.org/x/sys v0.0.0-20200223170610-d5e6a3e2c0ae // indirect
	gopkg.in/yaml.v2 v2.3.0 // indirect
)
