package network

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"os"
	"sync"
	"time"
)

var (
	rateLimiter = RateLimiter{expiresAt: time.Now()}
)

type Cat struct {
	ID  string `json:"id"`
	URL string `json:"url"`
}

type RateLimiter struct {
	mutex     sync.Mutex
	count     int
	expiresAt time.Time
}

type CatAPIService struct {
	httpClient HttpRequester
}

func NewCatAPIService(httpClient HttpRequester) CatAPIService {
	return CatAPIService{
		httpClient: httpClient,
	}
}

func (service CatAPIService) GetRandomCat() (Cat, error) {
	if hasReachedQuota() {
		return Cat{}, errors.New("quota_limit_reached")
	}

	requestURL := fmt.Sprintf("%s/v1/images/search?mime_types=image/png", os.Getenv("CAT_API_URL"))
	response, err := service.httpClient.Get(requestURL, map[string]string{"x-api-key": os.Getenv("CAT_API_KEY")})
	if err != nil {
		return Cat{}, nil
	}

	var catsResponse []Cat
	responseBytes, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return Cat{}, err
	}

	err = json.Unmarshal(responseBytes, &catsResponse)
	if err != nil {
		return Cat{}, err
	}

	if len(catsResponse) == 0 {
		return Cat{}, errors.New("unable to find a cat")
	}

	return catsResponse[0], nil
}

func hasReachedQuota() bool {
	if rateLimiter.expiresAt.After(time.Now()) {
		return true
	}

	rateLimiter.mutex.Lock()
	rateLimiter.count++
	if rateLimiter.count > 10 {
		rateLimiter.expiresAt = time.Now().Add(1 * time.Minute)
		rateLimiter.count = 0
		return true
	}

	rateLimiter.mutex.Unlock()
	return false
}
