package network

import "net/http"

type HttpRequester interface {
	Get(url string, headers map[string]string) (*http.Response, error)
}

type DefaultHttpClient struct{}

func NewDefaultHttpClient() *DefaultHttpClient {
	return &DefaultHttpClient{}
}

func (httpClient *DefaultHttpClient) Get(url string, headers map[string]string) (*http.Response, error) {
	client := &http.Client{}
	request, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return nil, err
	}

	for headerName, value := range headers {
		request.Header.Add(headerName, value)
	}

	response, err := client.Do(request)
	if err != nil {
		return nil, err
	}

	return response, nil
}
