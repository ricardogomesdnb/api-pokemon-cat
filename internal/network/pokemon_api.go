package network

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
)

type PokemonAPIResponse struct {
	ID      uint   `json:"id"`
	Height  int    `json:"height"`
	Weight  int    `json:"weight"`
	Name    string `json:"name"`
	Sprites struct {
		BackDefault  string `json:"back_default"`
		FrontDefault string `json:"front_default"`
		Other        struct {
			OfficialArtwork struct {
				FrontDefault string `json:"front_default"`
			} `json:"official-artwork"`
		} `json:"other"`
	} `json:"sprites"`
}

type Pokemon struct {
	ID       uint
	Name     string
	PhotoURL string
}

type PokemonService struct {
	httpClient HttpRequester
}

func NewPokemonService(httpClient HttpRequester) PokemonService {
	return PokemonService{
		httpClient: httpClient,
	}
}

func (service PokemonService) GetPokemonWithID(ID uint) (pokemon Pokemon, err error) {
	requestURL := fmt.Sprintf("%s/api/v2/pokemon/%d", os.Getenv("POKEMON_API_URL"), ID)

	response, err := service.httpClient.Get(requestURL, map[string]string{})
	if err != nil {
		return pokemon, err
	}

	var responseBodyDTO PokemonAPIResponse
	responseBytes, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return pokemon, err
	}

	err = json.Unmarshal(responseBytes, &responseBodyDTO)
	if err != nil {
		return pokemon, err
	}

	return Pokemon{
		ID:       responseBodyDTO.ID,
		Name:     responseBodyDTO.Name,
		PhotoURL: responseBodyDTO.Sprites.Other.OfficialArtwork.FrontDefault,
	}, nil
}
