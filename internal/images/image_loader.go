package images

import (
	"errors"
	"io/ioutil"
	"log"
	"math/rand"
	"path"
	"strings"
	"sync"

	"github.com/disintegration/imaging"
	"gitlab.com/ricardogomesdnb/api-pokemon-cat/internal/network"
)

var (
	pokemonLoadLockMap sync.Map
)

func (service ImageMergeService) asyncLoadImages(pokemonID uint) (pokemonImg PokemonImage, catImg CatImage, err error) {
	wg := sync.WaitGroup{}
	wg.Add(2)
	go func() {
		defer wg.Done()
		pokemonImg, err = service.loadPokemonImage(pokemonID)
	}()

	go func() {
		defer wg.Done()
		catImg, err = service.loadRandomCatImage()
	}()

	wg.Wait()
	return pokemonImg, catImg, err
}

func (service ImageMergeService) loadRandomCatImage() (img CatImage, err error) {
	randomCat, err := service.catAPIService.GetRandomCat()
	if err != nil {
		log.Println("Unable to fetch random cat from cats API", err)

		localRandomCatImg, err := service.loadRandomCatFromDisk()
		if err != nil {
			log.Println("Unable to load random cat locally", err)
			return img, err
		}

		return localRandomCatImg, nil
	}

	catImage := CatImage{Cat: randomCat}
	err = service.downloadImageIntoDisk(catImage.Cat.URL, catImage.LocalPath())
	if err != nil {
		return img, err
	}

	randomCatImageSource, err := imaging.Open(catImage.LocalPath())
	if err != nil {
		log.Println("Unable to load random cat picture", err)
		return img, err
	}

	return CatImage{
		Cat:         randomCat,
		ImageSource: randomCatImageSource,
	}, nil
}

func getRandomCatLocalImagePath() (string, error) {
	files, err := ioutil.ReadDir(catsLocalStoragePath())
	if err != nil {
		return "", err
	}

	validFileNames := []string{}

	for _, file := range files {
		if strings.HasPrefix(file.Name(), ".") {
			// Discard hidden files like .gitkeep
			continue
		}

		validFileNames = append(validFileNames, file.Name())
	}

	randomCatIndex := 0
	// Just a safe check because Intn panics if you give it a 0
	if len(validFileNames)-1 > 0 {
		randomCatIndex = rand.Intn(len(validFileNames) - 1)
	}

	for index, name := range validFileNames {
		if randomCatIndex == index {
			return catsLocalStoragePath() + "/" + name, nil
		}
	}

	return "", errors.New("no cats locally stored")
}

func (service ImageMergeService) loadRandomCatFromDisk() (img CatImage, err error) {
	randomCatLocalPath, err := getRandomCatLocalImagePath()
	if err != nil {
		return img, err
	}

	catImageSource, err := imaging.Open(randomCatLocalPath)
	if err != nil {
		log.Println("Unable to open local cat photo", randomCatLocalPath, err)
		return img, err
	}

	return CatImage{
		Cat: network.Cat{
			ID: strings.Replace(path.Base(randomCatLocalPath), ".png", "", 1),
		},
		ImageSource: catImageSource,
	}, nil
}

func (service ImageMergeService) loadPokemonImage(pokemonID uint) (img PokemonImage, err error) {
	img.Pokemon.ID = pokemonID
	if !isImageStored(img.LocalPath()) {
		// Lets lock by pokemonID so we can ensure that we only request once the pokemon API for this ID
		lockMutex, _ := pokemonLoadLockMap.LoadOrStore(pokemonID, &sync.Mutex{})
		mutex := lockMutex.(*sync.Mutex)
		mutex.Lock()

		pokemon, err := service.pokemonService.GetPokemonWithID(pokemonID)
		if err != nil {
			log.Println("Unable to fetch pokemon with id", pokemonID, err)
			mutex.Unlock()
			return img, err
		}

		img.Pokemon = pokemon
		err = service.downloadImageIntoDisk(img.Pokemon.PhotoURL, img.LocalPath())
		if err != nil {
			mutex.Unlock()
			return img, err
		}

		mutex.Unlock()
	}

	pokemonImageSource, err := imaging.Open(img.LocalPath())
	if err != nil {
		log.Println("Unable to open pokemon photo", img.LocalPath(), err)
		return img, err
	}

	img.ImageSource = pokemonImageSource

	return img, nil
}
