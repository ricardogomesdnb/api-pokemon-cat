package images

import (
	"image"
	"log"

	"github.com/disintegration/imaging"
	"gitlab.com/ricardogomesdnb/api-pokemon-cat/internal/network"
)

type ImageMergeService struct {
	pokemonService network.PokemonService
	catAPIService  network.CatAPIService
	httpClient     network.HttpRequester
}

type MergeCatPokemonImageRequest struct {
	PokemonID      uint
	MaxImageSizePx *uint
}

type CatPokemonMergeResult struct {
	PokemonImage       PokemonImage
	CatImage           CatImage
	ResultingImage     image.Image
	ResultingImagePath string
}

func NewImageMergeService() ImageMergeService {
	return ImageMergeService{
		pokemonService: network.NewPokemonService(network.NewDefaultHttpClient()),
		catAPIService:  network.NewCatAPIService(network.NewDefaultHttpClient()),
		httpClient:     network.NewDefaultHttpClient(),
	}
}

func (service ImageMergeService) MergeCatAndPokemon(request MergeCatPokemonImageRequest) (*CatPokemonMergeResult, error) {
	pokemonImg, catImg, err := service.asyncLoadImages(request.PokemonID)
	if err != nil {
		log.Println("unable to continue, one of the images failed to load", err)
		return nil, err
	}

	return service.mergeImages(catImg, pokemonImg, request.MaxImageSizePx)
}

func (service ImageMergeService) mergeImages(catImage CatImage, pokemonImage PokemonImage, maxImageSizePx *uint) (*CatPokemonMergeResult, error) {
	finalImagePath := buildImageResultPath(pokemonImage, catImage)

	if isImageStored(finalImagePath) {
		resultingImage, err := loadImageFromDisk(finalImagePath)
		if err == nil {
			return &CatPokemonMergeResult{
				PokemonImage:       pokemonImage,
				CatImage:           catImage,
				ResultingImage:     resultingImage,
				ResultingImagePath: finalImagePath,
			}, nil
		}
	}

	imageWidth := catImage.ImageSource.Bounds().Dx()
	imageHeight := catImage.ImageSource.Bounds().Dy()

	if maxImageSizePx != nil {
		imageWidth = int(*maxImageSizePx) + 1
		imageHeight = int(*maxImageSizePx)
	}

	withCatImageFilled := imaging.Fill(catImage.ImageSource, imageWidth, imageHeight, imaging.Center, imaging.Lanczos)

	pokemonImageSize := withCatImageFilled.Rect.Dy() / 5
	pokemonImageResized := imaging.Resize(pokemonImage.ImageSource, pokemonImageSize, pokemonImageSize, imaging.Linear)

	pokemonPositionX := withCatImageFilled.Rect.Min.X
	pokemonPositionY := withCatImageFilled.Rect.Max.Y - pokemonImageResized.Rect.Dy()

	catWithPokemonOnTopImage := imaging.Paste(withCatImageFilled, pokemonImageResized, image.Point{X: pokemonPositionX, Y: pokemonPositionY})

	result := CatPokemonMergeResult{
		PokemonImage:       pokemonImage,
		CatImage:           catImage,
		ResultingImage:     catWithPokemonOnTopImage,
		ResultingImagePath: finalImagePath,
	}

	err := storeMergeResultInDisk(result)
	if err != nil {
		log.Println("unable to store resulting image in disk", err)
		return nil, err
	}

	return &result, nil
}
