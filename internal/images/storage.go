package images

import (
	"fmt"
	"image"
	"io"
	"os"

	"github.com/disintegration/imaging"
)

const (
	ResultingImagesPath = "images/results"
)

func isImageStored(path string) bool {
	_, err := os.Stat(path)
	return !os.IsNotExist(err)
}

func loadImageFromDisk(path string) (image.Image, error) {
	return imaging.Open(path)
}

func storeMergeResultInDisk(result CatPokemonMergeResult) error {
	path := buildImageResultPath(result.PokemonImage, result.CatImage)
	return imaging.Save(result.ResultingImage, path)
}

func buildImageResultPath(pokemonImg PokemonImage, catImg CatImage) string {
	return fmt.Sprintf("%s/%s%d.png", ResultingImagesPath, catImg.Cat.ID, pokemonImg.Pokemon.ID)
}

func (service ImageMergeService) downloadImageIntoDisk(externalURL string, localPath string) error {
	response, err := service.httpClient.Get(externalURL, map[string]string{})
	if err != nil {
		return err
	}

	defer response.Body.Close()

	imageFile, err := os.Create(localPath)
	if err != nil {
		return err
	}

	defer imageFile.Close()

	_, err = io.Copy(imageFile, response.Body)
	if err != nil {
		return err
	}

	return nil
}
