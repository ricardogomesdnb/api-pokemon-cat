package images

import (
	"fmt"
	"image"
	"os"

	"gitlab.com/ricardogomesdnb/api-pokemon-cat/internal/network"
)

const (
	ImageKindPokemon = "pokemon2"
	ImageKindCat     = "cat"
)

type StoredImage interface {
	LocalPath() string
}

type CatImage struct {
	Cat         network.Cat
	ImageSource image.Image
}

type PokemonImage struct {
	Pokemon     network.Pokemon
	ImageSource image.Image
}

func (image CatImage) LocalPath() string {
	return fmt.Sprintf("%s/%s.png", catsLocalStoragePath(), image.Cat.ID)
}

func (image PokemonImage) LocalPath() string {
	return fmt.Sprintf("%s/%d.png", pokemonsLocalStoragePath(), image.Pokemon.ID)
}

func catsLocalStoragePath() string {
	return os.Getenv("CATS_LOCAL_STORAGE_PATH")
}

func pokemonsLocalStoragePath() string {
	return os.Getenv("POKEMONS_LOCAL_STORAGE_PATH")
}
