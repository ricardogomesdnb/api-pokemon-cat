package api

import (
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"gitlab.com/ricardogomesdnb/api-pokemon-cat/internal/images"
)

func HealthCheck(ctx *gin.Context) {
	// Just a simple 200 reply
	ctx.Data(http.StatusOK, gin.MIMEPlain, []byte{})
}

func GetPokemonByID(ctx *gin.Context) {
	var reqQueryParameters struct {
		MaxImageSizePx *uint `form:"max_image_size_px"`
	}

	if err := ctx.ShouldBindQuery(&reqQueryParameters); err != nil ||
		(reqQueryParameters.MaxImageSizePx != nil && *reqQueryParameters.MaxImageSizePx < 50) {
		ctx.JSON(http.StatusBadRequest, gin.H{
			"error_code":    "invalid_max_image_size_px",
			"error_message": "Max image size in pixels must be at least 50px",
		})
		return
	}

	pokemonIDString := ctx.Param("pokemonID")
	pokemonID, err := strconv.Atoi(pokemonIDString)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{
			"error_code":    "invalid_pokemon_id",
			"error_message": "Pokemon ID must be an integer number greater than 0",
		})
		return
	}

	imageComputeService := images.NewImageMergeService()
	result, err := imageComputeService.MergeCatAndPokemon(images.MergeCatPokemonImageRequest{
		PokemonID:      uint(pokemonID),
		MaxImageSizePx: reqQueryParameters.MaxImageSizePx,
	})

	if err != nil {
		ctx.JSON(http.StatusInternalServerError, gin.H{
			"error_code":    "failed_to_merge_images",
			"error_message": err.Error(),
		})
		return
	}

	ctx.File(result.ResultingImagePath)
}
